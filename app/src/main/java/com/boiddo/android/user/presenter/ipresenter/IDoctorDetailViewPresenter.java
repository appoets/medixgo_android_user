package com.boiddo.android.user.presenter.ipresenter;

import com.boiddo.android.user.model.dto.common.Provider;

/**
 * Created by Tranxit Technologies.
 */

public interface IDoctorDetailViewPresenter extends IPresenter {
    void showVideoAlert(Provider data);
    void makeVideoCall(Provider data);
    void scheduleAppointment(Integer serviceTypeId, Integer providerID, String date, String time, String broadcast, String payment_mode);
}
