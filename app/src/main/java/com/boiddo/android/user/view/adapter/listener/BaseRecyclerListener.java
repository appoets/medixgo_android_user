package com.boiddo.android.user.view.adapter.listener;

public interface BaseRecyclerListener<BRL> {

    void onClickItem(int pos, BRL data);

}
