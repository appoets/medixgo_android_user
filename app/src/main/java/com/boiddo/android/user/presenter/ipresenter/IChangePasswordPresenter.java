package com.boiddo.android.user.presenter.ipresenter;

import com.boiddo.android.user.model.dto.request.ChangePasswordRequest;

public interface IChangePasswordPresenter extends IPresenter {
    void goToLogin();
    void changePassword(ChangePasswordRequest request);
}