package com.boiddo.android.user.presenter.ipresenter;

import com.boiddo.android.user.model.dto.request.RatingRequest;

/**
 * Created by Tranxit Technologies.
 */

public interface IRatingPresenter extends IPresenter  {
    void postRating(RatingRequest request);
}
