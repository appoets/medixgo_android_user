package com.boiddo.android.user.model.webservice;


import com.boiddo.android.user.model.dto.common.videocalldata.Fcm;
import com.boiddo.android.user.model.dto.request.ChangePasswordRequest;
import com.boiddo.android.user.model.dto.request.InvoiceRequest;
import com.boiddo.android.user.model.dto.request.LoginRequest;
import com.boiddo.android.user.model.dto.request.MissedCallRequest;
import com.boiddo.android.user.model.dto.request.PaymentRequest;
import com.boiddo.android.user.model.dto.request.ProfileRequest;
import com.boiddo.android.user.model.dto.request.RatingRequest;
import com.boiddo.android.user.model.dto.request.RegisterRequest;
import com.boiddo.android.user.model.dto.request.ResetPasswordRequest;
import com.boiddo.android.user.model.dto.request.WalletRequest;
import com.boiddo.android.user.model.dto.response.AccessToken;
import com.boiddo.android.user.model.dto.response.BaseResponse;
import com.boiddo.android.user.model.dto.response.CardResponse;
import com.boiddo.android.user.model.dto.response.CheckStatusResponse;
import com.boiddo.android.user.model.dto.response.ForgotPasswordResponse;
import com.boiddo.android.user.model.dto.response.HelpResponse;
import com.boiddo.android.user.model.dto.response.HistoryResponse;
import com.boiddo.android.user.model.dto.response.InvoiceResponse;
import com.boiddo.android.user.model.dto.response.LoginResponse;
import com.boiddo.android.user.model.dto.response.NotificationResponse;
import com.boiddo.android.user.model.dto.response.ProfileResponse;
import com.boiddo.android.user.model.dto.response.ProviderResponse;
import com.boiddo.android.user.model.dto.response.RegisterResponse;
import com.boiddo.android.user.model.dto.response.ScheduleResponse;
import com.boiddo.android.user.model.dto.response.SendRequestResponse;
import com.boiddo.android.user.model.dto.response.ServicesResponse;
import com.boiddo.android.user.model.dto.response.VideoCallResponse;
import com.boiddo.android.user.model.dto.response.WalletResponse;

import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {

    @POST("/api/user/oauth/token")
    Call<LoginResponse> postLogin(@Body LoginRequest request);

    @GET("api/user/details")
    Call<ProfileResponse> getUserDetails();

    @GET("api/user/help")
    Call<HelpResponse> getHelpDetails();

    @POST("api/user/signup")
    Call<RegisterResponse> postRegister(@Body RegisterRequest request);

    @FormUrlEncoded
    @POST("api/user/forgot/password")
    Call<ForgotPasswordResponse> forgotPassword(@Field("email") String email);

    @POST("api/user/change/password")
    Call<BaseResponse> changePassword(@Body ChangePasswordRequest changePasswordRequest);

    @POST("api/user/reset/password")
    Call<BaseResponse> resetPassword(@Body ResetPasswordRequest resetPasswordRequest);

    @POST("api/user/add/money")
    Call<WalletResponse> addMoney(@Body WalletRequest walletRequest);

    @POST("api/user/update/profile")
    Call<ProfileResponse> updateProfile(ProfileRequest request);

    @Multipart
    @POST("api/user/update/profile")
    Call<ProfileResponse> postProfileUpdate(@PartMap HashMap<String, RequestBody> params, @Part MultipartBody.Part filePart);

    @GET("api/user/trips_val")
    Call<HistoryResponse> getHistory();

    @GET("api/user/providers")
    Call<ProviderResponse> getProviderList();

    @GET("api/user/providers")
    Call<ProviderResponse> searchProvider(@Query("search") String searchKey);

    @GET("api/user/service_providers")
    Call<ProviderResponse> searchProviderByService(@Query("service") String searchKey);

    @GET("api/user/services_val")
    Call<ServicesResponse> getServiceList();

    @GET("api/user/services_val")
    Call<ServicesResponse> searchServices(@Query("search") String searchKey);

    @FormUrlEncoded
    @POST("api/user/devicetoken")
    Call<BaseResponse> updateToken(@Field("device_token") String token);

    @Headers({"Content-Type: application/json", "Authorization: key=AIzaSyAm1fQ5lcaMfa8QyV9DNoapTXfBSajCQQw"})
    @POST("fcm/send")
    Call<BaseResponse> sendFCM(@Body Fcm fcm);

    @POST("api/user/invoice")
    Call<InvoiceResponse> requestInvoice(@Body InvoiceRequest request);

    @POST("api/user/payment")
    Call<BaseResponse> requestPayment(@Body PaymentRequest request);

    @POST("api/user/rate/provider")
    Call<BaseResponse> rateProvider(@Body RatingRequest request);

    @GET("/api/user/card")
    Call<CardResponse> getCardDetails();

    @DELETE("api/user/card/delete")
    Call<BaseResponse> deleteCard(@Query("card_id") String id);

   /* @FormUrlEncoded
    @POST("api/user/card")
    Call<BaseResponse> addCard(@Field("stripe_token") String email);*/

    @FormUrlEncoded
    @POST("api/user/card")
    Call<BaseResponse> addCard(@Field("cvv") String cvv,@Field("number") String number,@Field("exp_month") String exp_month,@Field("exp_year") String exp_year);

    @GET("/api/user/upcoming/trips")
    Call<List<NotificationResponse>> getNotification();

    @POST("/api/user/missed/call")
    Call<BaseResponse> sendMissedCall(@Body MissedCallRequest request);

    @GET("api/user/trips_val")
    Call<VideoCallResponse> getVideoCallList();

    @FormUrlEncoded
    @POST("api/user/appointment/history")
    Call<List<ScheduleResponse>> getScheduledList(@Field("type") Integer servicetype);

    @GET("/api/user/request/check")
    Call<CheckStatusResponse> checkStatusCall();

    @GET("/api/user/request/check/{id}")
    Call<CheckStatusResponse> checkStatusCallwithID(@Path("id") String id);

    @FormUrlEncoded
    @POST("/api/user/send/request")
    Call<SendRequestResponse> sendSpecialistRequest(@Field("service_type") Integer servicetype, @Field("schedule_date") String schedule_date,
                                                    @Field("schedule_time") String schedule_time, @Field("broadcast") String broadcast, @Field("payment_mode") String payment_mode);

    @FormUrlEncoded
    @POST("/api/user/send/request")
    Call<SendRequestResponse> sendIndividualRequest(@Field("service_type") Integer servicetype, @Field("provider_id") String provider_id,
                                                    @Field("schedule_date") String schedule_date, @Field("schedule_time") String schedule_time, @Field("broadcast") String broadcast, @Field("payment_mode") String payment_mode);

    @FormUrlEncoded
    @POST("/api/user/cancel/request")
    Call<BaseResponse> cancelRequest(@Field("request_id") String request_id);


    @GET("api/user/video/access/token")
    Call<AccessToken> getTwilloToken(@Query("room_id") Object obj, @Query("id") Object id,
                                     @Query("request_id") Object mReqId);


    @FormUrlEncoded
    @POST("/api/user/send/request")
    Call<SendRequestResponse> sendRequestVideoCall(@Field("service_type") Integer servicetype, @Field("provider_id") String provider_id,
                                                   @Field("schedule_date") String schedule_date, @Field("schedule_time") String schedule_time, @Field("broadcast") String broadcast);
}