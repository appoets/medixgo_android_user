package com.boiddo.android.user.view.activity;

import android.content.Intent;
import android.view.View;
import android.widget.EditText;

import com.boiddo.android.user.R;
import com.boiddo.android.user.presenter.OneTimePasswordPresenter;
import com.boiddo.android.user.presenter.ipresenter.IOneTimePasswordPresenter;
import com.boiddo.android.user.view.iview.IOneTimePasswordView;

import butterknife.BindView;
import butterknife.OnClick;

import static com.boiddo.android.user.ZtoidApplication.getApplicationInstance;

public class OneTimePasswordActivity extends BaseActivity<IOneTimePasswordPresenter> implements IOneTimePasswordView {

    @BindView(R.id.etOTP)
    EditText etOTP;

    @Override
    int attachLayout() {
        return R.layout.activity_onetime_password;
    }

    @Override
    IOneTimePasswordPresenter initialize() {
        return new OneTimePasswordPresenter(this);
    }

    @OnClick({R.id.btnSendOTP})
    public void OnClickView(View view){
        switch (view.getId()){
            case R.id.btnSendOTP:
                validateOTP();
                break;
        }
    }

    @Override
    public void setUp() {
        String OTP = getApplicationInstance().getOTP();
        etOTP.setText(OTP);
    }

    private void validateOTP() {
        String otp = etOTP.getText().toString().trim();
        if (otp.equalsIgnoreCase("")){
            showSnackBar(getString(R.string.please_enter_otp));
        }else{
            iPresenter.goToForgotChangePassword();
        }
    }

    @Override
    public void goToForgotChangePassword() {
            startActivity(new Intent(this,ForgotChangePasswordActivity.class));
    }
}
