package com.boiddo.android.user.view.iview;

import com.boiddo.android.user.presenter.ipresenter.IMakeCallPresenter;

/**
 * Created by Tranxit Technologies.
 */

public interface IMakeCallView extends IView<IMakeCallPresenter> {
        void setUp();
}
