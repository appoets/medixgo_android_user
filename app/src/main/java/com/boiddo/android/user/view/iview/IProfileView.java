package com.boiddo.android.user.view.iview;



import com.boiddo.android.user.model.dto.response.ProfileResponse;
import com.boiddo.android.user.presenter.ipresenter.IProfilePresenter;

public interface IProfileView extends IView<IProfilePresenter> {
    void updateUserDetails(ProfileResponse response);
}
