package com.boiddo.android.user.presenter;

import android.support.annotation.NonNull;

import com.boiddo.android.user.model.CustomException;
import com.boiddo.android.user.model.LoginModel;
import com.boiddo.android.user.model.ProfileModel;
import com.boiddo.android.user.model.dto.request.LoginRequest;
import com.boiddo.android.user.model.dto.response.LoginResponse;
import com.boiddo.android.user.model.dto.response.ProfileResponse;
import com.boiddo.android.user.model.listener.IModelListener;
import com.boiddo.android.user.presenter.ipresenter.ILoginPresenter;
import com.boiddo.android.user.view.iview.ILoginView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import static com.boiddo.android.user.ZtoidApplication.getApplicationInstance;


public class LoginPresenter extends BasePresenter<ILoginView> implements ILoginPresenter {

    public LoginPresenter(ILoginView iView) {
        super(iView);
    }

    @Override
    public void goToForgotPassword() {
        iView.goToForgotPassword();
    }

    @Override
    public void goToRegistration() {
        iView.goToRegistration();
    }

    @Override
    public void postLogin(LoginRequest request) {
        iView.showProgressbar();
        new LoginModel(new IModelListener<LoginResponse>() {
            @Override
            public void onSuccessfulApi(@NonNull LoginResponse response) {
                getApplicationInstance().setAccessToken(response.getAccessToken());
                getUserInfo();
            }

            @Override
            public void onSuccessfulApi(@NotNull List<LoginResponse> response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).postLogin(request);
    }

    private void getUserInfo() {
        new ProfileModel(new IModelListener<ProfileResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull ProfileResponse response) {
                getApplicationInstance().setCurrency(response.getCurrency());
                iView.dismissProgressbar();
                iView.goToHome();
            }

            @Override
            public void onSuccessfulApi(@NotNull List<ProfileResponse> response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).getUserDetails();
    }
}
