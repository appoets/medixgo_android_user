package com.boiddo.android.user.view.iview;

import com.boiddo.android.user.presenter.ipresenter.IChatPresenter;
import com.boiddo.android.user.presenter.ipresenter.IServiceFlowPresenter;

/**
 * Created by Tranxit Technologies.
 */

public interface IServiceFlow extends IView<IServiceFlowPresenter> {
        void setUp();
}
