package com.boiddo.android.user.presenter;

import android.os.Bundle;

import com.boiddo.android.user.model.CustomException;
import com.boiddo.android.user.model.ScheduleModel;
import com.boiddo.android.user.model.dto.response.ScheduleResponse;
import com.boiddo.android.user.model.listener.IModelListener;
import com.boiddo.android.user.presenter.ipresenter.ISchedulePresenter;
import com.boiddo.android.user.view.adapter.listener.IScheduleListener;
import com.boiddo.android.user.view.iview.IScheduleView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class SchedulePresenter extends BasePresenter<IScheduleView> implements ISchedulePresenter {

    private List<ScheduleResponse> list = new ArrayList<>();

    public SchedulePresenter(IScheduleView iView) {
        super(iView);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        iView.initSetUp();
    }

    IScheduleListener iScheduleListener = new IScheduleListener() {
        @Override
        public void onClickItem(int pos, ScheduleResponse data) {
            iView.moveToDetail(data);
        }
    };

    @Override
    public void getScheduledList() {
        iView.showProgressbar();
        new ScheduleModel(new IModelListener<ScheduleResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull ScheduleResponse response) {

            }

            @Override
            public void onSuccessfulApi(@NotNull List<ScheduleResponse> response) {
                iView.dismissProgressbar();
                iView.setAdapter(response, iScheduleListener);
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).getScheduleList();
    }
}
