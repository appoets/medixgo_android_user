package com.boiddo.android.user.view.iview;

import com.boiddo.android.user.model.dto.response.ScheduleResponse;
import com.boiddo.android.user.presenter.ipresenter.ISchedulePresenter;
import com.boiddo.android.user.view.adapter.listener.IScheduleListener;

import java.util.List;

public interface IScheduleView extends IView<ISchedulePresenter> {
    void setAdapter(List<ScheduleResponse> list, IScheduleListener iScheduleListener);
    void moveToDetail(ScheduleResponse data);
    void initSetUp();
}
