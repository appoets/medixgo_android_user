package com.boiddo.android.user.presenter.ipresenter;

import com.boiddo.android.user.model.dto.request.LoginRequest;
import com.boiddo.android.user.model.dto.request.RegisterRequest;

public interface IRegisterPresenter extends IPresenter {
    void goToLogin();
    void postLogin(LoginRequest request);
    void postRegister(RegisterRequest registerRequest);
}