package com.boiddo.android.user.presenter;

import android.os.Bundle;

import com.boiddo.android.user.model.CustomException;
import com.boiddo.android.user.model.HelpModel;
import com.boiddo.android.user.model.dto.response.HelpResponse;
import com.boiddo.android.user.model.listener.IModelListener;
import com.boiddo.android.user.presenter.ipresenter.IHelpPresenter;
import com.boiddo.android.user.view.iview.IHelpView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class HelpPresenter extends BasePresenter<IHelpView> implements IHelpPresenter {

    public HelpPresenter(IHelpView iView) {
        super(iView);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getHelpDetails();
    }

    @Override
    public void getHelpDetails() {
        new HelpModel(new IModelListener<HelpResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull HelpResponse response) {
                iView.updateHelpDetails(response);
            }

            @Override
            public void onSuccessfulApi(@NotNull List<HelpResponse> response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).getHelpDetails();
    }
}
