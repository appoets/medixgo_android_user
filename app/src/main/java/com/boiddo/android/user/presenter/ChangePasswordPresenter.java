package com.boiddo.android.user.presenter;


import com.boiddo.android.user.model.ChangePasswordModel;
import com.boiddo.android.user.model.CustomException;
import com.boiddo.android.user.model.dto.request.ChangePasswordRequest;
import com.boiddo.android.user.model.dto.response.BaseResponse;
import com.boiddo.android.user.model.listener.IModelListener;
import com.boiddo.android.user.presenter.ipresenter.IChangePasswordPresenter;
import com.boiddo.android.user.view.iview.IChangePasswordView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class ChangePasswordPresenter extends BasePresenter<IChangePasswordView> implements IChangePasswordPresenter {

    public ChangePasswordPresenter(IChangePasswordView iView) {
        super(iView);
    }

    @Override
    public void goToLogin() {
        iView.goToLogin();
    }

    @Override
    public void changePassword(ChangePasswordRequest request) {
        iView.showProgressbar();
        new ChangePasswordModel(new IModelListener<BaseResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull BaseResponse response) {
                iView.dismissProgressbar();
                iView.goToLogin();
            }

            @Override
            public void onSuccessfulApi(@NotNull List<BaseResponse> response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
                iView.dismissProgressbar();
            }
        }).updatePassword(request);
    }
}
