package com.boiddo.android.user.view.iview;

import com.boiddo.android.user.presenter.ipresenter.IEditProfilePresenter;

/**
 * Created by Tranxit Technologies.
 */

public interface IEditProfileView extends IView<IEditProfilePresenter> {
}
