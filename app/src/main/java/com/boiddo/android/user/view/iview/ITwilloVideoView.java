package com.boiddo.android.user.view.iview;

import com.boiddo.android.user.model.dto.common.Provider;
import com.boiddo.android.user.presenter.ipresenter.IDoctorDetailViewPresenter;
import com.boiddo.android.user.presenter.ipresenter.ITwilloVideoPresenter;

/**
 * Created by Tranxit Technologies.
 */

public interface ITwilloVideoView extends IView<ITwilloVideoPresenter> {
    void twiloVideoToken(Object token);
}
