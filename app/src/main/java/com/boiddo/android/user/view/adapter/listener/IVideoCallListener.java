package com.boiddo.android.user.view.adapter.listener;

import com.boiddo.android.user.model.dto.response.VideoCallResponse;

public interface IVideoCallListener extends BaseRecyclerListener<VideoCallResponse> {
}
