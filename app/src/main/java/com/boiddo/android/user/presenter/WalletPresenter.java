package com.boiddo.android.user.presenter;

import android.os.Bundle;

import com.boiddo.android.user.model.CustomException;
import com.boiddo.android.user.model.AddDeleteCardModel;
import com.boiddo.android.user.model.GetCardModel;
import com.boiddo.android.user.model.dto.response.BaseResponse;
import com.boiddo.android.user.model.dto.response.CardResponse;
import com.boiddo.android.user.model.listener.IModelListener;
import com.boiddo.android.user.presenter.ipresenter.IWalletPresenter;
import com.boiddo.android.user.view.iview.IWalletView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Created by Tranxit Technologies.
 */

public class WalletPresenter extends BasePresenter<IWalletView> implements IWalletPresenter {

    public WalletPresenter(IWalletView iView) {
        super(iView);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        iView.setUp();
    }

    @Override
    public void getCard() {
        iView.showProgressbar();
        new GetCardModel(new IModelListener<CardResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull CardResponse response) {
                iView.dismissProgressbar();
                iView.onSuccess(response.getCards());
            }

            @Override
            public void onSuccessfulApi(@NotNull List<CardResponse> response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.dismissProgressbar();
                iView.showNetworkMessage();
            }
        }).getCardDetail();
    }

    @Override
    public void addCard(String token) {

    }

    @Override
    public void addCard(String cvv,String number,String month,String year) {
        iView.showProgressbar();
        new AddDeleteCardModel(new IModelListener<BaseResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull BaseResponse response) {
                iView.dismissProgressbar();
                iView.onAddCard();
            }

            @Override
            public void onSuccessfulApi(@NotNull List<BaseResponse> response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.dismissProgressbar();
                iView.showNetworkMessage();
            }
        }).addCard(cvv,number,month,year);
    }

    @Override
    public void deleteCard(String cardId) {
        iView.showProgressbar();
        new AddDeleteCardModel(new IModelListener<BaseResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull BaseResponse response) {
                iView.dismissProgressbar();
                iView.onDeleteCard();
            }

            @Override
            public void onSuccessfulApi(@NotNull List<BaseResponse> response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.dismissProgressbar();
                iView.showNetworkMessage();
            }
        }).deleteCard(cardId);
    }
}
