package com.boiddo.android.user.presenter.ipresenter;

public interface IForgotPasswordPresenter extends IPresenter {
    void goToOneTimePassword();
    void getOTPDetails(String email);
}