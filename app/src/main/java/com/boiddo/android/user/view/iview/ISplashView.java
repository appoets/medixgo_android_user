package com.boiddo.android.user.view.iview;

import com.boiddo.android.user.presenter.ipresenter.ISplashPresenter;

public interface ISplashView extends IView<ISplashPresenter> {

    void startTimer(int splashTimer);

    void gotoLogin();

    void gotoHome();

}
