package com.boiddo.android.user.presenter.ipresenter;

public interface ISchedulePresenter extends IPresenter {
    void getScheduledList();
}
