package com.boiddo.android.user.view.iview;

import com.google.android.gms.maps.model.MarkerOptions;
import com.boiddo.android.user.model.dto.common.Provider;
import com.boiddo.android.user.presenter.HomeFragmentPresenter;

import java.util.List;

public interface IHomeFragementView extends IView<HomeFragmentPresenter> {
    void setMarkers(List<Provider> data);
    void drawOnMap(List<MarkerOptions>markerOptionsList);
    void moveToDetailView(Provider data);
}
