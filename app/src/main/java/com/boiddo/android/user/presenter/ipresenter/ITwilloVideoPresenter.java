package com.boiddo.android.user.presenter.ipresenter;

import com.boiddo.android.user.model.dto.common.Provider;

/**
 * Created by Tranxit Technologies.
 */

public interface ITwilloVideoPresenter extends IPresenter {
    void getTwiloToken(Object object);
}
