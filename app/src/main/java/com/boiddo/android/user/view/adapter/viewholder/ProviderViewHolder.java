package com.boiddo.android.user.view.adapter.viewholder;

import android.support.v7.widget.CardView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.boiddo.android.user.R;
import com.boiddo.android.user.common.Constants;
import com.boiddo.android.user.model.dto.common.Provider;
import com.boiddo.android.user.util.CodeSnippet;
import com.boiddo.android.user.view.adapter.listener.IProviderRecyclerAdapter;

import java.util.Random;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.boiddo.android.user.ZtoidApplication.getApplicationInstance;

/**
 * Created by Tranxit Technologies.
 */

public class ProviderViewHolder extends BaseViewHolder<Provider, IProviderRecyclerAdapter> {

    @BindView(R.id.civProfile)
    CircleImageView civProfile;

    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvSpl)
    TextView tvSpl;
    @BindView(R.id.tvFare)
    TextView tvFare;
    @BindView(R.id.rbRating)
    RatingBar rbRating;
    @BindView(R.id.ivVideoCall)
    ImageView ivVideoCall;
    @BindView(R.id.cv_switch)
    CardView cv_switch;

    private String currency;
    private int lastPosition = -1;


    public ProviderViewHolder(View itemView, IProviderRecyclerAdapter listener) {
        super(itemView, listener);
        currency = getApplicationInstance().getCurrency();
    }

    @Override
    void populateData(Provider data) {

        if (data != null) {
            tvName.setText(data.getFirstName() + " " + data.getLastName());

            String price = currency + data.getService().getProviderPrice() + "/min";
            if (data.getService().getProviderPrice()!=null)
                tvFare.setText(price);
            else
                tvFare.setText("-");


            if (data.getService().getServiceType() != null) {
                tvSpl.setText(data.getService().getServiceType().getName());
            } else {
                tvSpl.setText("-");
            }
            rbRating.setRating(Float.parseFloat(data.getRating()));
            String imageUrl = Constants.URL.BASE_URL_STORAGE + data.getAvatar();
            CodeSnippet.loadImageCircle(imageUrl, civProfile, R.drawable.ic_dummy_user);
        }

        ivVideoCall.setOnClickListener(v -> {
            listener.onVideoCall(data);
        });

        setAnimation(cv_switch, getAdapterPosition());
    }


    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            anim.setDuration(new Random().nextInt(501));//to make duration random number between [0,501)
            viewToAnimate.startAnimation(anim);
            lastPosition = position;
        }
    }

    @Override
    public void onClick(View view) {
        listener.onClickItem(getAdapterPosition(), data);
    }
}
