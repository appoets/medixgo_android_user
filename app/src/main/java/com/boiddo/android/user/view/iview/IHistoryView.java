package com.boiddo.android.user.view.iview;

import com.boiddo.android.user.model.dto.common.History;
import com.boiddo.android.user.presenter.ipresenter.IHistoryPresenter;
import com.boiddo.android.user.view.adapter.HistoryRecyclerAdater;

/**
 * Created by Tranxit Technologies.
 */

public interface IHistoryView extends IView<IHistoryPresenter> {
    void setAdapter(HistoryRecyclerAdater adapter);
    void moveToChat(History data);
    void initSetUp();
}
