package com.boiddo.android.user.presenter;

import android.os.Bundle;

import com.boiddo.android.user.model.CustomException;
import com.boiddo.android.user.model.HistoryModel;
import com.boiddo.android.user.model.dto.common.History;
import com.boiddo.android.user.model.dto.response.HistoryResponse;
import com.boiddo.android.user.model.listener.IModelListener;
import com.boiddo.android.user.presenter.ipresenter.IHistoryPresenter;
import com.boiddo.android.user.view.adapter.HistoryRecyclerAdater;
import com.boiddo.android.user.view.adapter.listener.IHistoryRecyclerAdapter;
import com.boiddo.android.user.view.iview.IHistoryView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Created by Tranxit Technologies.
 */

public class HistoryPresenter extends BasePresenter<IHistoryView> implements IHistoryPresenter {

    public HistoryPresenter(IHistoryView iView) {
        super(iView);
        getHistoryList();
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        iView.initSetUp();

    }

    IHistoryRecyclerAdapter iHistoryRecyclerAdapter = new IHistoryRecyclerAdapter() {
        @Override
        public void onClickItem(int pos, History data) {
            iView.moveToChat(data);
        }
    };

    @Override
    public void getHistoryList() {
        iView.showProgressbar();
        new HistoryModel(new IModelListener<HistoryResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull HistoryResponse response) {
                iView.dismissProgressbar();
                iView.setAdapter(new HistoryRecyclerAdater(response.getHistoryList(),iHistoryRecyclerAdapter));
            }

            @Override
            public void onSuccessfulApi(@NotNull List<HistoryResponse> response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).getHistoryList();
    }
}
