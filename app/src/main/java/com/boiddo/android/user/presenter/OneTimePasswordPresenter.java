package com.boiddo.android.user.presenter;

import com.boiddo.android.user.presenter.ipresenter.IOneTimePasswordPresenter;
import com.boiddo.android.user.view.iview.IOneTimePasswordView;


public class OneTimePasswordPresenter extends BasePresenter<IOneTimePasswordView> implements IOneTimePasswordPresenter {

    public OneTimePasswordPresenter(IOneTimePasswordView iView) {
        super(iView);
    }

    @Override
    public void goToForgotChangePassword() {
        iView.goToForgotChangePassword();
    }
}
