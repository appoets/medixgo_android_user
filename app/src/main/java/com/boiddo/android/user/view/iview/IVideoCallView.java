package com.boiddo.android.user.view.iview;

import com.boiddo.android.user.model.dto.common.History;
import com.boiddo.android.user.presenter.ipresenter.IVideoCallPresenter;
import com.boiddo.android.user.view.adapter.NotificationRecyclerAdapter;

public interface IVideoCallView extends IView<IVideoCallPresenter> {
    void setAdapter(NotificationRecyclerAdapter adapter);
    void moveToChat(History data);
    void initSetUp();
}
