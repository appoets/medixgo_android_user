package com.boiddo.android.user.view.adapter.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.boiddo.android.user.R;
import com.boiddo.android.user.util.CodeSnippet;
import com.boiddo.android.user.model.dto.common.History;
import com.boiddo.android.user.view.adapter.listener.IHistoryRecyclerAdapter;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Tranxit Technologies.
 */

public class HistoryViewHolder extends BaseViewHolder<History,IHistoryRecyclerAdapter> {

    @BindView(R.id.iv_profile)
    CircleImageView ivProfile;
    @BindView(R.id.tv_doctor_name)
    TextView tvDoctorName;
    @BindView(R.id.tv_doctor_spl)
    TextView tvDoctorSpl;
    @BindView(R.id.tv_date_time)
    TextView tvDateTime;
    @BindView(R.id.iv_chat)
    ImageView ivChat;


    public HistoryViewHolder(View itemView, IHistoryRecyclerAdapter listener) {
        super(itemView, listener);
    }

    @Override
    void populateData(History data) {
        String url = CodeSnippet.getImageURL(data.getProvider().getAvatar());
        showImage(ivProfile,url);
        tvDoctorName.setText(data.getProvider().getFirstName() + data.getProvider().getLastName());
        tvDoctorSpl.setText(data.getServiceType().getName());
        tvDateTime.setText(CodeSnippet.parseDateToyyyyMMdd(data.getAssignedAt()));
    }

    @Override
    public void onClick(View view) {
        listener.onClickItem(getAdapterPosition(), data);
    }
}
