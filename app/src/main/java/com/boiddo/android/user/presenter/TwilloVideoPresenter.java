package com.boiddo.android.user.presenter;

import android.os.Bundle;

import com.boiddo.android.user.model.CustomException;
import com.boiddo.android.user.model.SendRequestModel;
import com.boiddo.android.user.model.dto.common.Provider;
import com.boiddo.android.user.model.dto.response.SendRequestResponse;
import com.boiddo.android.user.model.listener.IModelListener;
import com.boiddo.android.user.presenter.ipresenter.IDoctorDetailViewPresenter;
import com.boiddo.android.user.presenter.ipresenter.ITwilloVideoPresenter;
import com.boiddo.android.user.view.iview.IDoctorDetailView;
import com.boiddo.android.user.view.iview.ITwilloVideoView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Created by Tranxit Technologies.
 */

public class TwilloVideoPresenter extends BasePresenter<ITwilloVideoView> implements ITwilloVideoPresenter {

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle != null) {
            Provider data = (Provider) bundle.getParcelable("data");
            //iView.setUp(data);
        }
    }

    public TwilloVideoPresenter(ITwilloVideoView iView) {
        super(iView);
    }


    @Override
    public void getTwiloToken(Object object) {
        iView.twiloVideoToken(object);
    }
}
