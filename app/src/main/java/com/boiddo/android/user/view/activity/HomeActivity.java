package com.boiddo.android.user.view.activity;

import android.app.FragmentManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.boiddo.android.user.R;
import com.boiddo.android.user.model.dto.response.ProfileResponse;
import com.boiddo.android.user.presenter.HomePresenter;
import com.boiddo.android.user.presenter.ipresenter.IHomePresenter;
import com.boiddo.android.user.util.CodeSnippet;
import com.boiddo.android.user.view.fragment.GoldenMinutesDialog;
import com.boiddo.android.user.view.fragment.HomeFragment;
import com.boiddo.android.user.view.iview.IHomeView;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class HomeActivity extends BaseActivity<IHomePresenter> implements IHomeView,
        NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @BindView(R.id.ivNotification)
    ImageView ivNotification;
    @BindView(R.id.tvNotificationCount)
    TextView tvNotificationCount;

    TextView tvName;
    CircleImageView civProfile;
    RelativeLayout rlHeaderMain;

    boolean doubleBackToExitPressedOnce = false;

    @Override
    int attachLayout() {
        return R.layout.activity_home;
    }

    @Override
    IHomePresenter initialize() {
        return new HomePresenter(this);
    }


    @Override
    protected void onResume() {
        super.onResume();
        iPresenter.getUserDetails();
        iPresenter.updateDeviceToken();
        iPresenter.getNotificationCount();
    }

    @Override
    public void setUp() {
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        tvName = header.findViewById(R.id.tvName);
        civProfile = header.findViewById(R.id.civProfile);
        rlHeaderMain = header.findViewById(R.id.rlHeaderMain);

        rlHeaderMain.setOnClickListener(v -> navigateTo(ProfileActivity.class, false, new Bundle()));

        /* Intial load fragment*/
        HomeFragment fragment = new HomeFragment();
        addFragment(new Bundle(), R.id.main_container, fragment, HomeFragment.TAG);


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            FragmentManager fragmentManager = getFragmentManager();
            if (fragmentManager.getBackStackEntryCount() > 1) {
                fragmentManager.popBackStack();
            } else {
                if (doubleBackToExitPressedOnce) {
                    finishAffinity();
                    return;
                }

                this.doubleBackToExitPressedOnce = true;
                showSnackBar("Please click BACK again to exit");

                new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            iPresenter.goToHome();
        } else if (id == R.id.nav_history) {
            iPresenter.goToHistory();
        } else if (id == R.id.nav_schedule) {
            iPresenter.goToSchedule();
        } else if (id == R.id.nav_videocall) {
            iPresenter.goToVideoCall();
        } else if (id == R.id.nav_help) {
            iPresenter.goToHelp();
        } else if (id == R.id.nav_wallet) {
            iPresenter.goToWallet();
        } else if (id == R.id.nav_logout) {
            showSnackBar("Logout clicked");
            iPresenter.onLogout();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void updateUserDetails(ProfileResponse response) {
        if (response != null) {
            tvName.setText(response.getFirstName());
            CodeSnippet.loadImageCircle(CodeSnippet.getImageURL(response.getPicture()), civProfile, R.drawable.ic_dummy_user);
        }
    }

    @Override
    public void goToHome() {
        HomeFragment fragment = new HomeFragment();
        changeFragment(new Bundle(), R.id.main_container, fragment, HomeFragment.TAG);
    }

    @Override
    public void goToHistory() {
        navigateTo(HistoryActivity.class, false, new Bundle());
    }

    @Override
    public void goToSchedule() {
        navigateTo(ScheduleActivity.class, false, new Bundle());
    }

    @Override
    public void goToVideoCall() {
        navigateTo(VideoCallActivity.class, false, new Bundle());
    }

    @Override
    public void goToHelp() {
        navigateTo(HelpActivity.class, false, new Bundle());
    }

    @Override
    public void goToWallet() {
        navigateTo(WalletActivity.class, false, new Bundle());
    }


    @OnClick({R.id.ivNotification, R.id.ivGolderMins})
    public void submit(View view) {
        switch (view.getId()) {
            case R.id.ivNotification:
                navigateTo(NotificationActivity.class, false, new Bundle());
                break;

            case R.id.ivGolderMins:
                iPresenter.showGoldenMins();
                break;
        }
    }


    @Override
    public void showGoldenMins() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        GoldenMinutesDialog.newInstance().show(getSupportFragmentManager(), "Dialog");
    }

    @Override
    public void updateNotification(int count) {
        if (count > 0) {
            if (count > 99)
                tvNotificationCount.setText("99+");
            else
                tvNotificationCount.setText(count + "");

            tvNotificationCount.setVisibility(View.VISIBLE);
            playAnimation();
        } else {
            tvNotificationCount.setVisibility(View.GONE);
        }
    }

    private void playAnimation() {
        YoYo.with(Techniques.Tada)
                .duration(700)
                .repeat(3)
                .playOn(ivNotification);
    }
}
