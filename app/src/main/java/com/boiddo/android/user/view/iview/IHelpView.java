package com.boiddo.android.user.view.iview;

import com.boiddo.android.user.model.dto.response.HelpResponse;
import com.boiddo.android.user.presenter.ipresenter.IHelpPresenter;

public interface IHelpView extends IView<IHelpPresenter> {
        void updateHelpDetails(HelpResponse response);
}
