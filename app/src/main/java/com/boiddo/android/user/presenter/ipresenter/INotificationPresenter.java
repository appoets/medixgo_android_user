package com.boiddo.android.user.presenter.ipresenter;

public interface INotificationPresenter extends IPresenter {

    void getNotification();
}
