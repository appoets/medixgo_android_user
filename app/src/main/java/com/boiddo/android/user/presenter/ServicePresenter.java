package com.boiddo.android.user.presenter;

import android.os.Bundle;


import com.boiddo.android.user.presenter.ipresenter.IServiceFlowPresenter;

import com.boiddo.android.user.view.iview.IServiceFlow;

public class ServicePresenter extends BasePresenter<IServiceFlow> implements IServiceFlowPresenter {

    public ServicePresenter(IServiceFlow iView) {
        super(iView);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        iView.setUp();
    }
}
