package com.boiddo.android.user.view.iview;


import com.boiddo.android.user.presenter.ipresenter.IChangePasswordPresenter;

public interface IChangePasswordView extends IView<IChangePasswordPresenter> {
                void goToLogin();
}
