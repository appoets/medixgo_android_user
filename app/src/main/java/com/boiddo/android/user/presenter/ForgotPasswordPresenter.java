package com.boiddo.android.user.presenter;

import com.boiddo.android.user.model.CustomException;
import com.boiddo.android.user.model.ForgotPasswordModel;
import com.boiddo.android.user.model.dto.response.ForgotPasswordResponse;
import com.boiddo.android.user.model.listener.IModelListener;
import com.boiddo.android.user.presenter.ipresenter.IForgotPasswordPresenter;
import com.boiddo.android.user.view.iview.IForgotPasswordView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import static com.boiddo.android.user.ZtoidApplication.getApplicationInstance;


public class ForgotPasswordPresenter extends BasePresenter<IForgotPasswordView> implements IForgotPasswordPresenter {


    @Override
    public void goToOneTimePassword() {
        iView.goToOneTimePassword();
    }

    public ForgotPasswordPresenter(IForgotPasswordView iView) {
        super(iView);
    }

    @Override
    public void getOTPDetails(String email){
        iView.showProgressbar();
        new ForgotPasswordModel(new IModelListener<ForgotPasswordResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull ForgotPasswordResponse response) {
                iView.dismissProgressbar();
                getApplicationInstance().setOTP(response.getUser().getOtp()+"");
                iView.goToOneTimePassword();
            }

            @Override
            public void onSuccessfulApi(@NotNull List<ForgotPasswordResponse> response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
                iView.dismissProgressbar();
            }
        }).getOTPDetails(email);
    }
}
