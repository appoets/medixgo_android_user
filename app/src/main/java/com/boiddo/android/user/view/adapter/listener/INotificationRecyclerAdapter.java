package com.boiddo.android.user.view.adapter.listener;

import com.boiddo.android.user.model.dto.response.NotificationResponse;

/**
 * Created by Tranxit Technologies.
 */

public interface INotificationRecyclerAdapter extends BaseRecyclerListener<NotificationResponse> {
}
