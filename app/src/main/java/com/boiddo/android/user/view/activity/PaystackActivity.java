package com.boiddo.android.user.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.boiddo.android.user.R;
import com.boiddo.android.user.util.CustomDialog;
import com.braintreepayments.cardform.view.CardForm;


import co.paystack.android.Paystack;
import co.paystack.android.PaystackSdk;
import co.paystack.android.Transaction;
import co.paystack.android.model.Card;
import co.paystack.android.model.Charge;

public class PaystackActivity extends AppCompatActivity {

    CardForm cardForm;
    Button submit;
    ImageView backArrow;

    Double amount;
    String requestId, email;
    CustomDialog customDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
            getWindow().setNavigationBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paystack);

        backArrow = findViewById(R.id.backArrow);
        cardForm = findViewById(R.id.card_form);
        submit = findViewById(R.id.submit);
        customDialog = new CustomDialog(PaystackActivity.this);
        customDialog.setCancelable(false);

        cardForm.cardRequired(true)
                .expirationRequired(true)
                .cvvRequired(true)
                .postalCodeRequired(false)
                .mobileNumberRequired(false)
                .actionLabel(getString(R.string.add_card_details))
                .setup(this);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            amount = extras.getDouble("amount", 0);
            requestId = extras.getString("request_id");
            email = extras.getString("email");
            //submit.setText(getString(R.string.pay_, SharedHelper.getKey(activity(), "currency") + " "+String.valueOf(amount)));
        }

        backArrow.setOnClickListener(view -> finish());

        submit.setOnClickListener(view -> {
            if (cardForm.getCardNumber().isEmpty()) {
                Toast.makeText(PaystackActivity.this, getString(R.string.please_enter_card_number), Toast.LENGTH_SHORT).show();
                return;
            }
            if (cardForm.getExpirationMonth().isEmpty()) {
                Toast.makeText(PaystackActivity.this, getString(R.string.please_enter_card_expiration_details), Toast.LENGTH_SHORT).show();
                return;
            }
            if (cardForm.getCvv().isEmpty()) {
                Toast.makeText(PaystackActivity.this, getString(R.string.please_enter_card_cvv), Toast.LENGTH_SHORT).show();
                return;
            }
            if (!TextUtils.isDigitsOnly(cardForm.getExpirationMonth()) || !TextUtils.isDigitsOnly(cardForm.getExpirationYear())) {
                Toast.makeText(PaystackActivity.this, getString(R.string.please_enter_card_expiration_details), Toast.LENGTH_SHORT).show();
                return;
            }
            if ((customDialog != null))
                customDialog.show();
            String cardNumber = cardForm.getCardNumber();
            int cardMonth = Integer.parseInt(cardForm.getExpirationMonth());
            int cardYear = Integer.parseInt(cardForm.getExpirationYear());
            String cardCvv = cardForm.getCvv();
            Log.d("CARD", "CardDetails Number: " + cardNumber + "Month: " + cardMonth + " Year: " + cardYear + " Cvv " + cardCvv);
            Card card = new Card(cardNumber, cardMonth, cardYear, cardCvv);
            if (card.isValid()) {
                performCharge(card);
            } else {
                Toast.makeText(PaystackActivity.this, "Invalid card", Toast.LENGTH_SHORT).show();
            }
        });

    }

    void performCharge(Card card) {
        Charge charge = new Charge();
        charge.setEmail(email);
        charge.setAmount((int) Math.round(amount));
        charge.setCard(card); //sets the card to charge

        PaystackSdk.chargeCard(this, charge, new Paystack.TransactionCallback() {
            @Override
            public void onSuccess(Transaction transaction) {
                // This is called only after transaction is deemed successful.
                // Retrieve the transaction, and send its reference to your server
                // for verification.
                Log.d("paystack", transaction.getReference());
                if ((customDialog != null) && customDialog.isShowing())
                    customDialog.dismiss();
                Intent intent = new Intent();
                intent.putExtra("reference", transaction.getReference());
                intent.putExtra("request_id", requestId);
                intent.putExtra("amount", amount);
                intent.putExtra("amount", amount);
                setResult(Activity.RESULT_OK, intent);
                finish();
            }

            @Override
            public void beforeValidate(Transaction transaction) {
                // This is called only before requesting OTP.
                // Save reference so you may send to server. If
                // error occurs with OTP, you should still verify on server.
                if ((customDialog != null) && customDialog.isShowing())
                    customDialog.dismiss();
            }

            @Override
            public void onError(Throwable error, Transaction transaction) {
                if ((customDialog != null) && customDialog.isShowing())
                    customDialog.dismiss();
                //handle error here
                Toast.makeText(PaystackActivity.this, error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                Log.d("paystack", error.getLocalizedMessage());
            }

        });
    }

}
