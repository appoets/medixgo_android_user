package com.boiddo.android.user.view.iview;

import com.boiddo.android.user.presenter.ipresenter.IOneTimePasswordPresenter;

public interface IOneTimePasswordView extends IView<IOneTimePasswordPresenter> {
        void goToForgotChangePassword();
        void setUp();
}
