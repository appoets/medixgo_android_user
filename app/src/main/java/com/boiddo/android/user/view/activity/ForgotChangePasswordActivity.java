package com.boiddo.android.user.view.activity;

import android.content.Intent;
import android.view.View;

import com.boiddo.android.user.R;
import com.boiddo.android.user.presenter.ForgotChangePasswordPresenter;
import com.boiddo.android.user.presenter.ipresenter.IForgotChangePasswordPresenter;
import com.boiddo.android.user.view.iview.IForgotChangePasswordView;

import butterknife.OnClick;

public class ForgotChangePasswordActivity extends BaseActivity<IForgotChangePasswordPresenter> implements IForgotChangePasswordView {

    @Override
    int attachLayout() {
        return R.layout.activity_forgot_change_password;
    }

    @Override
    IForgotChangePasswordPresenter initialize() {
        return new ForgotChangePasswordPresenter(this);
    }



    @OnClick({R.id.btnChangePassword})
    public void OnClickView(View view){
        switch (view.getId()){

            case R.id.btnChangePassword:
                iPresenter.goToLogin();
                break;
        }
    }


    @Override
    public void goToLogin() {
        startActivity(new Intent(this,LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }


}
