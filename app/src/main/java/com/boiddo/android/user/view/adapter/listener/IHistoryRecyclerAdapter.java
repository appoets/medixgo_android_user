package com.boiddo.android.user.view.adapter.listener;

import com.boiddo.android.user.model.dto.common.History;

/**
 * Created by Tranxit Technologies.
 */

public interface IHistoryRecyclerAdapter extends BaseRecyclerListener<History> {
}
