package com.boiddo.android.user.view.iview;

import com.boiddo.android.user.presenter.ipresenter.IRegisterPresenter;

public interface IRegisterView extends IView<IRegisterPresenter> {
    void goToLogin();
    void goToHome();
}
