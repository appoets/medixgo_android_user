package com.boiddo.android.user.view.adapter.listener;

import com.boiddo.android.user.model.dto.response.ScheduleResponse;

public interface IScheduleListener extends BaseRecyclerListener<ScheduleResponse> {
}
