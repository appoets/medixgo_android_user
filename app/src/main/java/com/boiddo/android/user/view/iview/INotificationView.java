package com.boiddo.android.user.view.iview;

import com.boiddo.android.user.presenter.ipresenter.INotificationPresenter;
import com.boiddo.android.user.view.adapter.NotificationRecyclerAdapter;

public interface INotificationView extends IView<INotificationPresenter> {
    void initSetUp();
    void setAdapter(NotificationRecyclerAdapter adapter);
}
