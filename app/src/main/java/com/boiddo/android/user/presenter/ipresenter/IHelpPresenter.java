package com.boiddo.android.user.presenter.ipresenter;

public interface IHelpPresenter extends IPresenter {
    void getHelpDetails();
}