package com.boiddo.android.user.view.iview;

import com.boiddo.android.user.model.dto.common.Provider;
import com.boiddo.android.user.presenter.ipresenter.IDoctorDetailViewPresenter;

/**
 * Created by Tranxit Technologies.
 */

public interface IDoctorDetailView extends IView<IDoctorDetailViewPresenter> {
    void setUp(Provider data);
    void showVideoAlert(Provider data);
    void makeVideoCall(Provider data);
    void startCheckStatus(String request_id);
}
