package com.boiddo.android.user.presenter;

import android.os.Bundle;

import com.boiddo.android.user.presenter.ipresenter.IChatPresenter;
import com.boiddo.android.user.view.iview.IChatView;

public class ChatPresenter extends BasePresenter<IChatView> implements IChatPresenter {

    public ChatPresenter(IChatView iView) {
        super(iView);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        iView.setUp();
    }
}
