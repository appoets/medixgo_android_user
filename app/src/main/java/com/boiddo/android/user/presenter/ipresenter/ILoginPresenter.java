package com.boiddo.android.user.presenter.ipresenter;

import com.boiddo.android.user.model.dto.request.LoginRequest;

public interface ILoginPresenter extends IPresenter {
    void postLogin(LoginRequest request);
    void goToRegistration();
    void goToForgotPassword();
}