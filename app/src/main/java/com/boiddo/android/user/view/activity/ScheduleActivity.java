package com.boiddo.android.user.view.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.TextView;

import com.boiddo.android.user.R;
import com.boiddo.android.user.model.dto.response.ScheduleResponse;
import com.boiddo.android.user.presenter.SchedulePresenter;
import com.boiddo.android.user.presenter.ipresenter.ISchedulePresenter;
import com.boiddo.android.user.view.adapter.ScheduleAdapter;
import com.boiddo.android.user.view.adapter.listener.IScheduleListener;
import com.boiddo.android.user.view.iview.IScheduleView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;

public class ScheduleActivity extends BaseActivity<ISchedulePresenter> implements IScheduleView {

    @BindView(R.id.ibBack)
    ImageView ibBack;
    @BindView(R.id.rcvScheduledList)
    RecyclerView rcvScheduledList;
    @BindView(R.id.calendarView)
    CalendarView calendarView;
    @BindView(R.id.noData)
    TextView noData;

    private String selectedDate;
    //private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault());
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    private ScheduleAdapter adapter;
    private List<ScheduleResponse> wholeresponse = new ArrayList<>();
    private List<ScheduleResponse> selectedDateList;
    private String tempDate;
    private IScheduleListener iScheduleListener;


    @Override
    int attachLayout() {
        return R.layout.activity_schedule;
    }

    @Override
    ISchedulePresenter initialize() {
        selectedDate = dateFormat.format(Calendar.getInstance().getTime());
        return new SchedulePresenter(this);
    }

    /*    @Override
    public void setAdapter(ScheduleAdapter adapter, IScheduleListener iScheduleListener) {
        if (adapter.getItemCount() > 0) {
            noData.setVisibility(View.GONE);
            rcvScheduledList.setVisibility(View.VISIBLE);
            rcvScheduledList.setAdapter(adapter);
        } else {
            rcvScheduledList.setVisibility(View.GONE);
            noData.setVisibility(View.VISIBLE);
        }
    }*/

    @OnClick({R.id.ibBack})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibBack:
                finish();
                break;
            default:
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        iPresenter.getScheduledList();
    }

    @Override
    public void setAdapter(List<ScheduleResponse> list, IScheduleListener iScheduleListener) {
        if (list.size() > 0) {
            wholeresponse = list;
            this.iScheduleListener = iScheduleListener;
            searchForSelectedDate();
            if (selectedDateList.size() > 0) {
                noData.setVisibility(View.GONE);
                rcvScheduledList.setVisibility(View.VISIBLE);
                adapter = new ScheduleAdapter(selectedDateList, iScheduleListener);
                rcvScheduledList.setAdapter(adapter);
            } else {
                rcvScheduledList.setVisibility(View.GONE);
                noData.setVisibility(View.VISIBLE);
            }
        } else {
            rcvScheduledList.setVisibility(View.GONE);
            noData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void moveToDetail(ScheduleResponse data) {

        if (data.getStatus().equalsIgnoreCase("CANCELLED")) {
            showToast("Request was Cancelled");
        }/* else if (data.getStatus().equalsIgnoreCase("COMPLETED")) {

        } else if (data.getStatus().equalsIgnoreCase("SCHEDULED")) {

        }*/ else {
            Intent intent = new Intent(ScheduleActivity.this, ScheduleDetailActivity.class);
            intent.putExtra("id", ""+data.getId());
            intent.putExtra("status", data.getStatus());
            startActivity(intent);

        }
    }

    @Override
    public void initSetUp() {
        rcvScheduledList.setLayoutManager(new LinearLayoutManager(getActivity()));
        rcvScheduledList.setItemAnimator(new DefaultItemAnimator());

        calendarView.setDate(Calendar.getInstance().getTimeInMillis(), false, true);

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                try {
                    //selectedDate = year + "-" + (month + 1) + "-" + dayOfMonth;
                    Date enddateObj = dateFormat.parse(year + "-" + (month + 1) + "-" + dayOfMonth);
                    selectedDate = dateFormat.format(enddateObj);
                    //selectedDate = dateFormat.format(selectedDate);
                }catch (Exception e) {
                    e.printStackTrace();
                }
                if (wholeresponse.size() > 0)
                    getSelectedDateList();
            }
        });
    }

    private void getSelectedDateList() {
        searchForSelectedDate();
        if (selectedDateList.size() > 0) {
            adapter = new ScheduleAdapter(selectedDateList, iScheduleListener);
            rcvScheduledList.setAdapter(adapter);
            noData.setVisibility(View.GONE);
            rcvScheduledList.setVisibility(View.VISIBLE);
        } else {
            rcvScheduledList.setVisibility(View.GONE);
            noData.setVisibility(View.VISIBLE);
        }
    }

        private void searchForSelectedDate() {
        tempDate = "";
        selectedDateList = new ArrayList<>();
        try {
            for (int i = 0; i < wholeresponse.size(); i++) {
                if (wholeresponse.get(i).getScheduleAt() != null && !wholeresponse.get(i).getScheduleAt().equalsIgnoreCase(null)) {
                    Date enddateObj = dateFormat.parse(wholeresponse.get(i).getScheduleAt().split(" ")[0]);
                    tempDate = dateFormat.format(enddateObj);
                    if (tempDate.equalsIgnoreCase(selectedDate)) {
                        selectedDateList.add(wholeresponse.get(i));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
       /* if (requestCode == PICK_PAYSTACK && resultCode == Activity.RESULT_OK && data != null) {
            //payNowPayStack(data.getStringExtra("reference"));
            payNowAPICall(data.getStringExtra("reference"));
        }*/
    }

}
