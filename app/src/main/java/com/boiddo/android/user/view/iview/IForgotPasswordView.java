package com.boiddo.android.user.view.iview;

import com.boiddo.android.user.presenter.ipresenter.IForgotPasswordPresenter;

public interface IForgotPasswordView extends IView<IForgotPasswordPresenter> {
        void goToOneTimePassword();
}
