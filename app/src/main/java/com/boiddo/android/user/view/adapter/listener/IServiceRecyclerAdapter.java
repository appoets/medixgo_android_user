package com.boiddo.android.user.view.adapter.listener;

import com.boiddo.android.user.model.dto.common.Services;

/**
 * Created by Tranxit Technologies.
 */

public interface IServiceRecyclerAdapter extends BaseRecyclerListener<Services> {
}
