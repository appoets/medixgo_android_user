package com.boiddo.android.user.presenter;

import android.os.Bundle;

import com.boiddo.android.user.model.CustomException;
import com.boiddo.android.user.model.NotificationModel;
import com.boiddo.android.user.model.dto.response.NotificationResponse;
import com.boiddo.android.user.model.listener.IModelListener;
import com.boiddo.android.user.presenter.ipresenter.IVideoCallPresenter;
import com.boiddo.android.user.view.adapter.NotificationRecyclerAdapter;
import com.boiddo.android.user.view.adapter.listener.INotificationRecyclerAdapter;
import com.boiddo.android.user.view.iview.IVideoCallView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class VideoCallPresenter extends BasePresenter<IVideoCallView> implements IVideoCallPresenter {

    public VideoCallPresenter(IVideoCallView iView) {
        super(iView);
        getVideoCallList();
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        iView.initSetUp();
    }

    INotificationRecyclerAdapter iNotificationRecyclerAdapter = new INotificationRecyclerAdapter() {
        @Override
        public void onClickItem(int pos, NotificationResponse data) {

        }
    };

    @Override
    public void getVideoCallList() {
        iView.showProgressbar();
/*
        new VideoCallModel(new IModelListener<VideoCallResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull VideoCallResponse response) {
                iView.dismissProgressbar();
                iView.setAdapter(new VideoCallAdapter(new ArrayList<>()*/
/*response*//*
, iVideoCallListener));
            }

            @Override
            public void onSuccessfulApi(@NotNull List<VideoCallResponse> response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).getVideoCallList();
*/

        new NotificationModel(new IModelListener<NotificationResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull List<NotificationResponse> response) {
                iView.dismissProgressbar();
                iView.setAdapter(new NotificationRecyclerAdapter(response,iNotificationRecyclerAdapter));
            }

            @Override
            public void onSuccessfulApi(@NotNull NotificationResponse response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).getNotificationDetail();

    }
}
