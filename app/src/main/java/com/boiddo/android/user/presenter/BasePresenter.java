package com.boiddo.android.user.presenter;

import android.content.Intent;
import android.os.Bundle;

import com.boiddo.android.user.model.CustomException;
import com.boiddo.android.user.presenter.ipresenter.IPresenter;
import com.boiddo.android.user.view.activity.LoginActivity;
import com.boiddo.android.user.view.activity.WelcomeScreenActivity;
import com.boiddo.android.user.view.iview.IView;

import static com.boiddo.android.user.ZtoidApplication.getApplicationInstance;

public abstract class BasePresenter<V extends IView> implements IPresenter {
    public final String TAG = getClass().getSimpleName();

    protected Bundle bundle;

    protected V iView;

    public BasePresenter(V iView) {
        this.iView = iView;
    }

    @Override
    public void onCreate(Bundle bundle) {
        this.bundle = bundle;
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onActivityForResult(int requestCode, int resultCode, Intent data) {
    }

    @Override
    public String getStringRes(int resId) {
        return iView.getActivity().getString(resId);
    }


    @Override
    public void onLogout() {
        getApplicationInstance().logout();
        iView.navigateTo(WelcomeScreenActivity.class, false, new Bundle());
        iView.getActivity().finishAffinity();
    }

    @Override
    public void onLogout(CustomException e) {

    }


}
