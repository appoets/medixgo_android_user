package com.boiddo.android.user.presenter.ipresenter;

public interface ISplashPresenter extends IPresenter {

    boolean onCheckUserStatus();

    boolean hasInternet();

    void goToHome();

    void goToLogin();

}