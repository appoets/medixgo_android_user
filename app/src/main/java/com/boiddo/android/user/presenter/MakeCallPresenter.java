package com.boiddo.android.user.presenter;

import android.os.Bundle;

import com.boiddo.android.user.model.CustomException;
import com.boiddo.android.user.model.FCMModel;
import com.boiddo.android.user.model.dto.common.videocalldata.Fcm;
import com.boiddo.android.user.model.dto.response.BaseResponse;
import com.boiddo.android.user.model.listener.IModelListener;
import com.boiddo.android.user.presenter.ipresenter.IMakeCallPresenter;
import com.boiddo.android.user.view.iview.IMakeCallView;

import org.jetbrains.annotations.NotNull;

import java.util.List;


public class MakeCallPresenter extends BasePresenter<IMakeCallView> implements IMakeCallPresenter {

    public MakeCallPresenter(IMakeCallView iView) {
        super(iView);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        iView.setUp();
    }

    @Override
    public void sendFCMMessage(Fcm data) {
        new FCMModel(new IModelListener<BaseResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull BaseResponse response) {
                iView.dismissProgressbar();
                //iView.showToast("GCM Send Successfully");
            }

            @Override
            public void onSuccessfulApi(@NotNull List<BaseResponse> response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
               // iView.showToast("GCM failed");
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.makeLogout();
               // iView.showToast("GCM failed authorization");
            }

            @Override
            public void onNetworkFailure() {
                iView.dismissProgressbar();
                iView.showNetworkMessage();
            }
        }).sendFCM(data);
    }
}
