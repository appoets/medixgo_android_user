package com.boiddo.android.user.presenter.ipresenter;

import com.boiddo.android.user.model.dto.common.videocalldata.Fcm;

public interface IMakeCallPresenter extends IPresenter {
    void sendFCMMessage(Fcm data);
}