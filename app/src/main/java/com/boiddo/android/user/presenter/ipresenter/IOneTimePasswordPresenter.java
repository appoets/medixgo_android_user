package com.boiddo.android.user.presenter.ipresenter;

public interface IOneTimePasswordPresenter extends IPresenter {
    void goToForgotChangePassword();
}