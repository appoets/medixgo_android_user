package com.boiddo.android.user.view.iview;

import com.boiddo.android.user.model.dto.response.Cards;
import com.boiddo.android.user.presenter.ipresenter.IWalletPresenter;

/**
 * Created by Tranxit Technologies.
 */

public interface IWalletView extends IView<IWalletPresenter> {
    void setUp();
    void onSuccess(Cards cards);
    void onAddCard();
    void onDeleteCard();
}
