package com.boiddo.android.user.view.adapter.listener;

import com.boiddo.android.user.model.dto.common.Provider;

/**
 * Created by Tranxit Technologies.
 */

public interface IProviderRecyclerAdapter extends BaseRecyclerListener<Provider> {
    void onVideoCall(Provider data);
}
