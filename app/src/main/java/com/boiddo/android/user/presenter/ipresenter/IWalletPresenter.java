package com.boiddo.android.user.presenter.ipresenter;

/**
 * Created by Tranxit Technologies.
 */

public interface IWalletPresenter extends IPresenter {
    void getCard();
    void addCard(String token);
    void addCard(String cvv,String number,String month,String year);
    void deleteCard(String cardId);
}
