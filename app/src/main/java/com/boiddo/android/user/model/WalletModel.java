package com.boiddo.android.user.model;

import com.boiddo.android.user.model.dto.request.WalletRequest;
import com.boiddo.android.user.model.dto.response.WalletResponse;
import com.boiddo.android.user.model.listener.IModelListener;
import com.boiddo.android.user.model.webservice.ApiClient;
import com.boiddo.android.user.model.webservice.ApiInterface;

import java.util.List;

/**
 * Created by Tranxit Technologies.
 */

public class WalletModel extends BaseModel<WalletResponse> {

    public WalletModel(IModelListener<WalletResponse> listener) {
        super(listener);
    }

    public void addMoneyToWallet(WalletRequest walletRequest) {
        enQueueTask(new ApiClient().getClient().create(ApiInterface.class).addMoney(walletRequest));
    }

    @Override
    public void onSuccessfulApi(WalletResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onSuccessfulApi(List<WalletResponse> response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }
}
